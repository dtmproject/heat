/**
 * @file main.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-17, heat, UK
 * @date 2015-02-26, app, UK
 *
 * @brief DTM++/APP: A minimal FEM problem for deal.II.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/APP.                                           */
/*                                                                            */
/*  DTM++/APP is free software: you can redistribute it and/or modify         */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/APP is distributed in the hope that it will be useful,              */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/APP.   If not, see <http://www.gnu.org/licenses/>.       */

// PROJECT includes
#include <heat/minimal.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes
#include <iostream>
#include <fstream>


int main() {
	try {
		////////////////////////////////////////////////////////////////////////
		// Init application
		//
		
		//
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		// Begin application
		//
		
		heat::Minimal<2> problem_2d;
		problem_2d.run();
		
		//
		// End application
		////////////////////////////////////////////////////////////////////////
	}
	catch (std::exception &exc) {
		std::cerr	<< std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl
					<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
					<< std::endl;
		
		std::cerr	<< exc.what() << std::endl;
		
		std::cerr	<< std::endl
					<< "APPLICATION TERMINATED unexpectedly due to an exception."
					<< std::endl << std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl;
		
		return 1;
	}
	catch (...) {
		std::cerr	<< std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl
					<< "An UNKNOWN EXCEPTION occured!"
					<< std::endl;
		
		std::cerr	<< std::endl
					<< "----------------------------------------"
					<< "----------------------------------------"
					<< std::endl << std::endl
					<< "Further information:" << std::endl
					<< "\tThe main() function catched an exception"
					<< std::endl
					<< "\twhich is not inherited from std::exception."
					<< std::endl
					<< "\tYou have probably called 'throw' somewhere,"
					<< std::endl
					<< "\tif you do not have done this, please contact the authors!"
					<< std::endl << std::endl
					<< "----------------------------------------"
					<< "----------------------------------------"
					<< std::endl;
		
		std::cerr	<< std::endl
					<< "APPLICATION TERMINATED unexpectedly due to an exception."
					<< std::endl << std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl;
		
		return 1;
	}
	
	return 0;
}
