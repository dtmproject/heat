/**
 * @file minimal.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-26, UK
 *
 * @brief DTM++/APP: A minimal FEM problem for deal.II.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/APP.                                           */
/*                                                                            */
/*  DTM++/APP is free software: you can redistribute it and/or modify         */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/APP is distributed in the hope that it will be useful,              */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/APP.   If not, see <http://www.gnu.org/licenses/>.       */

/* ---------------------------------------------------------------------
 *
 * Copyright (C) 1999 - 2014 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------
 */

// PROJECT includes
#include <heat/minimal.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/function.h>
#include <deal.II/base/function_parser.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_update_flags.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/grid/grid_generator.h>

#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/linear_operator.h>
#include <deal.II/lac/packaged_operation.h>
#include <deal.II/lac/solver_selector.h>
#include <deal.II/lac/solver_control.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/vector_tools.h>

// C++ includes
#include <fstream>
#include <vector>

namespace heat {

template<int dim>
void
Minimal<dim>::
init_mesh() {
	const double a = 0.;
	const double b = 1.;
	const double global_refinement(5);
	
	////////////////////////////////////////////////////////////////////////////
	// Create mesh
	//
	Assert(tria.use_count(), dealii::ExcNotInitialized());
	dealii::GridGenerator::hyper_cube(*tria,a,b);
	
	tria->refine_global(global_refinement);
	
	////////////////////////////////////////////////////////////////////////////
	// Colorise boundary faces
	//
	auto cell = tria->begin_active();
	auto endc = tria->end();
	for ( ; cell != endc ; ++cell) {
		for (unsigned int face_id(0);
			face_id < dealii::GeometryInfo<dim>::faces_per_cell; ++face_id ) {
			if (cell->face(face_id)->at_boundary()) {
				cell->face(face_id)->set_boundary_id(
					static_cast< dealii::types::boundary_id > (heat::boundary_id::Dirichlet)
				);
			}
		}
	}
}

template<int dim>
void
Minimal<dim>::
distribute_dofs_and_make_constraints() {
	////////////////////////////////////////////////////////////////////////////
	// Distribute dofs
	//
	Assert(dof.use_count(), dealii::ExcNotInitialized());
	Assert(fe.use_count(), dealii::ExcNotInitialized());
	dof->distribute_dofs(*fe);
	
	////////////////////////////////////////////////////////////////////////////
	// Make constraints
	//
	if (!constraints.use_count()) {
		constraints = std::make_shared< dealii::ConstraintMatrix > ();
	}
	
	Assert(constraints.use_count(), dealii::ExcNotInitialized());
	constraints->clear();
	constraints->reinit();
	
	dealii::DoFTools::make_hanging_node_constraints(*dof, *constraints);
	dealii::DoFTools::make_zero_boundary_constraints(*dof, *constraints);
	
	constraints->close();
	
// 	constraints->print(std::cout);
}


template<int dim>
void
Minimal<dim>::
create_sparsity_pattern() {
	////////////////////////////////////////////////////////////////////////////
	// Create sparsity pattern
	// cf. for details: deal.II step-2 tutorial
	Assert(dof.use_count(), dealii::ExcNotInitialized());
	dealii::DynamicSparsityPattern dsp(
		dof->n_dofs(),
		dof->n_dofs()
	);
	
	dealii::DoFTools::make_sparsity_pattern(*dof,dsp);
// 	dsp.print(std::cout);
// 	std::cout << std::endl;
	
	if (!sp.use_count()) {
		sp = std::make_shared< dealii::SparsityPattern > ();
	}
	
	Assert(sp.use_count(), dealii::ExcNotInitialized());
	sp->copy_from(dsp);	
// 	sp->print(std::cout);
}


template<int dim>
void
Minimal<dim>::
init() {
	////////////////////////////////////////////////////////////////////////////
	// Check if everything is initialised and output some statistics
	//
	Assert(tria.use_count(), dealii::ExcNotInitialized());
	Assert(dof.use_count(), dealii::ExcNotInitialized());
	Assert(sp.use_count(), dealii::ExcNotInitialized());
	
	dealii::deallog << "mesh cells: \t" << tria->n_active_cells() << std::endl;
	dealii::deallog << "DoFs: \t\t" << dof->n_dofs() << std::endl;
	
	////////////////////////////////////////////////////////////////////////////
	// Initialise matrices and vectors
	//
	M.reinit(*sp);
	A.reinit(*sp);
	
	u0.reinit(dof->n_dofs());
	u1.reinit(dof->n_dofs());
	
	f0.reinit(dof->n_dofs());
	f1.reinit(dof->n_dofs());
}


template<int dim>
void
Minimal<dim>::
assemble_system() {
	////////////////////////////////////////////////////////////////////////////
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	// NOTE: see deal.II step-3 tutorial for details
	// Here, we want to assemble the system matrix of the HeatEquation.
	// Due to FEM, this can be done by summing up local contributions over
	// all cells and trial/test function combinations.
	// The inner most integral will be solved through a numical approximation
	// by a Gaussian quadrature rule.
	
	M = 0;
	A = 0;
	
	// Setup a Gaussian quadrature formula
	// NOTE: We take p+1 quadrature points for each dimension in space
	//
	Assert(fe.use_count(), dealii::ExcNotInitialized());
	dealii::QGauss<dim> quad(fe->tensor_degree()+1);
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement on the
	// current cell.
	//
	Assert(fe.use_count(), dealii::ExcNotInitialized());
	dealii::UpdateFlags uflags =
		dealii::update_values |
		dealii::update_gradients |
		dealii::update_JxW_values;
	
	mapping = std::make_shared< dealii::MappingQ<dim> > (
		fe->tensor_degree()
	);
	
	dealii::FEValues<dim> fe_values(
		*mapping,
		*fe,
		quad,
		uflags
	);
	
	// Setup a small full matrix, to store the assembly on each mesh cell
	// efficiently.
	// Afterwards they will be distributed into the global (sparse) matrix.
	//
	Assert(tria.use_count(), dealii::ExcNotInitialized());
	dealii::FullMatrix<double> mass_assembly_on_cell(
		fe->dofs_per_cell,
		fe->dofs_per_cell
	);
	
	dealii::FullMatrix<double> stiffness_assembly_on_cell(
		fe->dofs_per_cell,
		fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indicies.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs each cell has.
	//
	Assert(fe.use_count(), dealii::ExcNotInitialized());
	std::vector<dealii::types::global_dof_index> local_dof_indicies(fe->dofs_per_cell);
	
	////////////////////////////////////////////////////////////////////////////
	// Assembly loop
	//
	// Now we do the real work, looping over all cells to compute the cell
	// assemblys. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	// We initialise it with the first active cell of our triangulation.
	//
	Assert(tria.use_count(), dealii::ExcNotInitialized());
	auto cell = dof->begin_active();
	auto endc = dof->end();
	
	for ( ; cell != endc; ++cell) {
		// First we have to compute the values of the gradients and
		// the JxW values.
		// The reinit of the fe_values object on the current cell will do this.
		fe_values.reinit(cell);
		
		mass_assembly_on_cell = 0;
		stiffness_assembly_on_cell = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < fe->dofs_per_cell; ++i)
		for (unsigned int j(0); j < fe->dofs_per_cell; ++j)
		for (unsigned int q(0); q < quad.size(); ++q) {
			mass_assembly_on_cell(i,j) +=
				fe_values.shape_value(i,q) * fe_values.shape_value(j,q) *
				fe_values.JxW(q);
			
			stiffness_assembly_on_cell(i,j) +=
				fe_values.shape_grad(i,q) * fe_values.shape_grad(j,q) *
				fe_values.JxW(q);
		}
		
		// Store the global indicies into the vector local_dof_indicies.
		// The cell object will give us the information.
		Assert(
			(local_dof_indicies.size() == fe->dofs_per_cell),
			dealii::ExcNotInitialized()
		);
		cell->get_dof_indices(local_dof_indicies);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		Assert(constraints.use_count(), dealii::ExcNotInitialized());
		
		constraints->distribute_local_to_global(
			mass_assembly_on_cell,
			local_dof_indicies, local_dof_indicies,
			M
		);
		
		constraints->distribute_local_to_global(
			stiffness_assembly_on_cell,
			local_dof_indicies, local_dof_indicies,
			A
		);
	}
	
// 	std::cout << "M = " << std::endl;
// 	M.print_formatted(std::cout,2);
// 	std::cout << std::endl;
// 	
// 	std::cout << "A = " << std::endl;
// 	A.print_formatted(std::cout,2);
// 	std::cout << std::endl;
}


template<int dim>
void
Minimal<dim>::
assemble_rhs(const double t1) {
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	// NOTE: see deal.II step-3 tutorial for details
	
	f1 = 0;
	
	Assert(fun_f.use_count(), dealii::ExcNotInitialized());
	fun_f->set_time(t1);
	
	// Setup a Gaussian quadrature formula
	// NOTE: We take p+1 quadrature points for each dimension in space
	//
	Assert(fe.use_count(), dealii::ExcNotInitialized());
	dealii::QGauss<dim> quad(fe->tensor_degree()+1);
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement on the
	// current cell.
	//
	Assert(fe.use_count(), dealii::ExcNotInitialized());
	
	dealii::UpdateFlags uflags =
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values;
	
	mapping = std::make_shared< dealii::MappingQ<dim> > (
		fe->tensor_degree()
	);
	
	dealii::FEValues<dim> fe_values(
		*mapping,
		*fe,
		quad,
		uflags
	);
	
	// Setup a small vector, to store the assembly on each mesh cell
	// efficiently.
	// Afterwards they will be distributed into the global vector f1.
	Assert(fe.use_count(), dealii::ExcNotInitialized());
	dealii::Vector<double> force_assembly_on_cell(fe->dofs_per_cell);
	double fun_f_value(0);
	
	// Setup a small vector, to store the global dof indicies.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs each cell has.
	//
	Assert(fe.use_count(), dealii::ExcNotInitialized());
	std::vector<dealii::types::global_dof_index> local_dof_indicies(fe->dofs_per_cell);
	
	////////////////////////////////////////////////////////////////////////////
	// Assembly loop
	//
	// Now we do the real work, looping over all cells to compute the cell
	// assemblys. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	// We initialise it with the first active cell of our triangulation.
	//
	Assert(tria.use_count(), dealii::ExcNotInitialized());
	auto cell = dof->begin_active();
	auto endc = dof->end();
	
	for ( ; cell != endc; ++cell) {
		fe_values.reinit(cell);
		
		force_assembly_on_cell = 0;
		
		// Now we loop over all test functions and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < fe->dofs_per_cell; ++i)
		for (unsigned int q(0); q < quad.size(); ++q) {
			Assert(fun_f.use_count(), dealii::ExcNotInitialized());
			fun_f_value = fun_f->value(fe_values.quadrature_point(q), 0);
			
			force_assembly_on_cell(i) +=
				fun_f_value * fe_values.shape_value(i,q) * fe_values.JxW(q);
		}
		
		// Store the global indicies into the vector local_dof_indicies.
		// The cell object will give us the information.
		Assert(
			(local_dof_indicies.size() == fe->dofs_per_cell),
			dealii::ExcNotInitialized()
		);
		cell->get_dof_indices(local_dof_indicies);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		Assert(constraints.use_count(), dealii::ExcNotInitialized());
		
		constraints->distribute_local_to_global(
			force_assembly_on_cell,
			local_dof_indicies,
			f1
		);
	}
}

template<int dim>
void
Minimal<dim>::
run () {
	const double _t0 = 0; // initial time
	const double _T = 1.; // final time
	tau_n = 1e-2;         // global time step length
	
	const unsigned int p = 1; // polynomial degree p in space (FE_Q(p)-FE)
	
	////////////////////////////////////////////////////////////////////////////
	// Force function
	{
		fun_f = std::make_shared< dealii::ConstantFunction<dim> > (-10.);
	}
	
	////////////////////////////////////////////////////////////////////////////
	// Initial value function
	{
		auto u0_fun = std::make_shared< dealii::FunctionParser<dim> >(1);
		const std::string variables = "x0,x1";
		const std::string expression = "x0*(x0-1)*x1*(x1-1)*(2*2*2*2)";
		const std::map<std::string, double> constants;
		u0_fun->initialize(variables,expression,constants);
		
		fun_u0 = u0_fun;
	}
	
	////////////////////////////////////////////////////////////////////////////
	vtk_file_counter = 0;
	
	////////////////////////////////////////////////////////////////////////////
	// Programme initialisation
	//
	fe = std::make_shared< dealii::FE_Q<dim> > (p);
	tria = std::make_shared< dealii::Triangulation<dim> > ();
	dof = std::make_shared< dealii::DoFHandler<dim> > (*tria);
	
	////////////////////////////////////////////////////////////////////////////
	init_mesh();
	distribute_dofs_and_make_constraints();
	create_sparsity_pattern();
	init();
	
	////////////////////////////////////////////////////////////////////////////
	assemble_system();
	const auto lop_M = dealii::linear_operator(M);
	const auto lop_A = dealii::linear_operator(A);
	
	////////////////////////////////////////////////////////////////////////////
	// Initialise time marching loop
	//
// 	double t0(-1);
	double t1(_t0);
	
	////////////////////////////////////////////////////////////////////////////
	// interpolate initial values onto grid / into initial solutions
	u1 = 0;
	dealii::VectorTools::interpolate(
		*mapping,
		*dof,
		*fun_u0,
		u1
	);
	write_solution_as_vtk();
	
	f1 = 0;
	assemble_rhs(t1);
	
	////////////////////////////////////////////////////////////////////////////
	// Create linear system solver
	//
	auto solver_control = std::make_shared< dealii::SolverControl > (
		1000, 1e-10, false, true
	);
	auto linear_system_solver = std::make_shared< dealii::SolverCG< > > (
		*solver_control
	);
	
	////////////////////////////////////////////////////////////////////////////
	// Time marching loop
	//
	for (unsigned int n = 0; (_t0 + n*tau_n) < _T; ++n ) {
// 		t0 = t1;
		t1 = _t0 + n*tau_n;
		u0 = u1;
		f0 = f1;
		
		// assemble f1
		assemble_rhs(t1);
		
		// solve
		linear_system_solver->solve(
			(lop_M + tau_n/2 * lop_A),                          // matrix object
			u1,                                                 // solution vector
			((tau_n/2)*(f0+f1) + (lop_M - tau_n/2 * lop_A)*u0), // rhs object
			dealii::PreconditionIdentity()                      // preconditioner object
		);
		
		write_solution_as_vtk();
	}
	
	////////////////////////////////////////////////////////////////////////////
	if (dof.use_count()) dof->clear();
}


template<int dim>
void
Minimal<dim>::
write_solution_as_vtk() {
	std::string name="solution";
	std::ostringstream filename;
	filename << name << "_" << std::setw(4) << std::setfill('0') << vtk_file_counter << ".vtk";
	
	std::ofstream output(filename.str().c_str());
	
	dealii::DataOut<dim> data_out;
	Assert(dof.use_count(), dealii::ExcNotInitialized());
	data_out.attach_dof_handler(*dof);
	data_out.add_data_vector(u1, name);
	data_out.build_patches(fe->tensor_degree());
	data_out.write_vtk(output);
	
	output.close();
	
	++vtk_file_counter;
}

} // namespaces

// Instanciation includes
#include "minimal.inst.in"
