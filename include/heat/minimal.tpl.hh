/**
 * @file minimal.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-02-26, UK
 *
 * @brief DTM++/APP: A minimal FEM problem for deal.II.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/APP.                                           */
/*                                                                            */
/*  DTM++/APP is free software: you can redistribute it and/or modify         */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/APP is distributed in the hope that it will be useful,              */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/APP.   If not, see <http://www.gnu.org/licenses/>.       */

/* ---------------------------------------------------------------------
 *
 * Copyright (C) 1999 - 2014 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------
 */

#ifndef minimal_tpl_hh
#define minimal_tpl_hh

#include <deal.II/base/function.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe.h>

#include <deal.II/grid/tria.h>

#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/sparsity_pattern.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>

namespace heat {
	enum class boundary_id {
		Dirichlet
	};

	template<int dim>
	class Minimal {
	public:
		Minimal() = default;
		virtual ~Minimal() = default;
		
		virtual void init_mesh();
		virtual void distribute_dofs_and_make_constraints();
		virtual void create_sparsity_pattern();
		
		virtual void init();
		virtual void assemble_system();
		virtual void assemble_rhs(const double t1);
		
		virtual void run();
		
	protected:
		std::shared_ptr< dealii::Function<dim> > fun_u0;
		std::shared_ptr< dealii::Function<dim> > fun_f;
		
		std::shared_ptr< dealii::FiniteElement<dim> > fe;
		std::shared_ptr< dealii::Mapping<dim> > mapping;
		std::shared_ptr< dealii::Triangulation<dim> > tria;
		std::shared_ptr< dealii::DoFHandler<dim> > dof;
		
		std::shared_ptr< dealii::ConstraintMatrix > constraints;
		
		std::shared_ptr< dealii::SparsityPattern > sp;
		
		dealii::SparseMatrix<double> M, A;
		dealii::Vector<double> u0,u1;
		dealii::Vector<double> f0,f1;
		
		double tau_n;
		
		virtual void write_solution_as_vtk();
		unsigned int vtk_file_counter;
	};

} // namespaces

#endif
